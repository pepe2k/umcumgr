/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * References:
 *   - https://docs.zephyrproject.org/latest/services/device_mgmt/smp_protocol.html
 *     subsys/mgmt/mcumgr/lib/mgmt/include/mgmt/mgmt.h
 *         (zephyrproject-rtos/zephyr@github)
 *   - mgmt/include/mgmt/mgmt.h (apache/mynewt-mcumgr@github)
 *   - transport/smp-console.md (apache/mynewt-mcumgr@github)
 */

#ifndef __SMP_H
#define __SMP_H

#include <libubox/utils.h>

int smp_mgmt_confirm(const char *hash, bool confirm);
int smp_mgmt_echo(const char *text);
int smp_mgmt_list(void);
int smp_mgmt_select(int slot);
int smp_mgmt_simple(uint8_t mgmt_op, uint16_t mgmt_grp, uint8_t mgmt_id);
int smp_mgmt_sysinfo(void);
int smp_mgmt_upload(FILE *image, int image_len, int image_num);

/** Opcodes; encoded in first byte of header. */
#define MGMT_OP_READ		0
#define MGMT_OP_READ_RSP	1
#define MGMT_OP_WRITE		2
#define MGMT_OP_WRITE_RSP	3

/**
 * The first 64 groups are reserved for system level mcumgr commands.
 * Per-user commands are then defined after group 64.
 */
#define MGMT_GROUP_ID_OS		0
#define MGMT_GROUP_ID_IMAGE		1
#define MGMT_GROUP_ID_STAT		2
#define MGMT_GROUP_ID_CONFIG		3
#define MGMT_GROUP_ID_LOG		4
#define MGMT_GROUP_ID_CRASH		5
#define MGMT_GROUP_ID_SPLIT		6
#define MGMT_GROUP_ID_RUN		7
#define MGMT_GROUP_ID_FS		8
#define MGMT_GROUP_ID_SHELL		9
#define MGMT_GROUP_ID_PERUSER		64
#define MGMT_GROUP_ID_OWRT		28535

#define MGMT_OS_CMD_ID_ECHO		0
#define MGMT_OS_CMD_ID_CONSECHO		1
#define MGMT_OS_CMD_ID_STATS		2
#define MGMT_OS_CMD_ID_MEMPOOL		3
#define MGMT_OS_CMD_ID_DATETIME		4
#define MGMT_OS_CMD_ID_SYSRST		5
#define MGMT_OS_CMD_ID_MGRPARAM		6

#define MGMT_IMG_CMD_ID_LIST		0
#define MGMT_IMG_CMD_ID_UPLOAD		1
#define MGMT_IMG_CMD_ID_FILE		2
#define MGMT_IMG_CMD_ID_CORELIST	3
#define MGMT_IMG_CMD_ID_CORELOAD	4
#define MGMT_IMG_CMD_ID_ERASE		5

#define MGMT_OWRT_CMD_ID_SYSINFO	0
#define MGMT_OWRT_CMD_ID_BOOT		1
#define MGMT_OWRT_CMD_ID_SLOTSEL	2

/**
 * mcumgr error codes.
 */
#define MGMT_ERR_EOK		0
#define MGMT_ERR_EUNKNOWN	1
#define MGMT_ERR_ENOMEM		2
#define MGMT_ERR_EINVAL		3
#define MGMT_ERR_ETIMEOUT	4
#define MGMT_ERR_ENOENT		5
#define MGMT_ERR_EBADSTATE	6	/* Current state disallows command. */
#define MGMT_ERR_EMSGSIZE	7	/* Response too large. */
#define MGMT_ERR_ENOTSUP	8	/* Command not supported. */
#define MGMT_ERR_ECORRUPT	9	/* Corrupt */
#define MGMT_ERR_EBUSY		10	/* Command blocked by processing of other command */
#define MGMT_ERR_EPERUSER	256

#define MGMT_HDR_SIZE		8

struct mgmt_hdr {
#if __BYTE_ORDER == __LITTLE_ENDIAN
	uint8_t  nh_op:3;		/* MGMT_OP_[...] */
	uint8_t  _res1:5;
#elif __BYTE_ORDER == __BIG_ENDIAN
	uint8_t  _res1:5;
	uint8_t  nh_op:3;		/* MGMT_OP_[...] */
#endif
	uint8_t  nh_flags;		/* Reserved for future flags */
	uint16_t nh_len;		/* Length of the payload */
	uint16_t nh_group;		/* MGMT_GROUP_ID_[...] */
	uint8_t  nh_seq;		/* Sequence number */
	uint8_t  nh_id;			/* Message ID within group */
};

#endif
