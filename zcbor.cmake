#
# Generated using zcbor version 0.8.1-0.8.1
# https://github.com/NordicSemiconductor/zcbor
# Generated with a --default-max-qty of 3
#

add_library(zcbor)
target_sources(zcbor PRIVATE
    ${CMAKE_CURRENT_LIST_DIR}/zcbor/zcbor_decode.c
    ${CMAKE_CURRENT_LIST_DIR}/zcbor/zcbor_encode.c
    ${CMAKE_CURRENT_LIST_DIR}/zcbor/zcbor_common.c
    )
target_include_directories(zcbor PUBLIC
    ${CMAKE_CURRENT_LIST_DIR}/zcbor
    )
