/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright (C) 2023 Piotr Dymacz <pepe2k@gmail.com>
 */

#ifndef __TISBL_H
#define __TISBL_H

#include <stdint.h>

#if defined(TI_SERIAL_BL_DUMPCMD)
int tisbl_dump(FILE *image);
#endif
int tisbl_reset(void);
int tisbl_upload(FILE *image, int image_len);

#define CC26XX_FLASH_SECTOR_SIZE	(8 * 1024)
#define CC26XX_FLASH_SIZE_REG		0x4003002c
#define CC26XX_FLASH_START_ADDR		0x00000000

/* ACK/NACK */
#define TISBL_RESPONSE_ACK		0xcc
#define TISBL_RESPONSE_NAK		0x33

/* Supported commands (constant length) */
#define TISBL_CMD_DUMMY			0x00
#define TISBL_CMD_DUMMY_LEN		3

#define TISBL_CMD_PING			0x20
#define TISBL_CMD_PING_LEN		3

#define TISBL_CMD_DOWNLOAD		0x21
#define TISBL_CMD_DOWNLOAD_LEN		11

#define TISBL_CMD_GET_STATUS		0x23
#define TISBL_CMD_GET_STATUS_LEN	3

#define TISBL_CMD_RESET			0x25
#define TISBL_CMD_RESET_LEN		3

#define TISBL_CMD_SECTOR_ERASE		0x26
#define TISBL_CMD_SECTOR_ERASE_LEN	7

#define TISBL_CMD_CRC32			0x27
#define TISBL_CMD_CRC32_LEN		15

#define TISBL_CMD_GET_CHIP_ID		0x28
#define TISBL_CMD_GET_CHIP_ID_LEN	3

#define TISBL_CMD_MEM_READ		0x2a
#define TISBL_CMD_MEM_READ_LEN		9

#define TISBL_CMD_BANK_ERASE		0x2c
#define TISBL_CMD_BANK_ERASE_LEN	3

#define TISBL_CMD_SET_CCFG		0x2d
#define TISBL_CMD_SET_CCFG_LEN		11

#define TISBL_CMD_DOWNLOAD_CRC		0x2f
#define TISBL_CMD_DOWNLOAD_CRC_LEN	15

/* Supported commands (variable length) */
#define TISBL_CMD_SEND_DATA		0x24	/* packet length 4~255 */
#define TISBL_CMD_MEM_WRITE		0x2b	/* packet length 9~255 */

/* TISBL_CMD_GET_STATUS reply values */
#define TISBL_STATUS_SUCCESS		0x40
#define TISBL_STATUS_UNK_CMD		0x41
#define TISBL_STATUS_INV_CMD		0x42
#define TISBL_STATUS_INV_ADR		0x43
#define TISBL_STATUS_FLASH_FAIL		0x44

struct __attribute__((__packed__)) tisbl_packet_hdr {
	uint8_t size;
	uint8_t checksum;
};

struct __attribute__((__packed__)) tisbl_cmd_simple {
	struct tisbl_packet_hdr hdr;
	uint8_t command;
};

/* COMMAND_CRC32 flow:
 * >>> cmd
 * <<< ack
 * <<< 6-byte packet with 4-byte CRC (in BE)
 * >>> ack
 * */
struct __attribute__((__packed__)) tisbl_cmd_crc32 {
	struct tisbl_packet_hdr hdr;
	uint8_t command;
	uint32_t data_addr;
	uint32_t data_size;
	uint32_t repeat_cnt;
};

/* COMMAND_DOWNLOAD_CRC flow:
 * >>> cmd
 * <<< ack/nak
 * >>> status get
 * <<< ack/nak
 * <<< 3-byte packet with status of last command
 * >>> ack/nak
 */
struct __attribute__((__packed__)) tisbl_cmd_dl_crc {
	struct tisbl_packet_hdr hdr;
	uint8_t command;
	uint32_t data_addr;
	uint32_t data_size;
	uint32_t crc32;
};

/* COMMAND_MEMORY_READ flow:
 * >>> cmd
 * <<< ack/nak
 * <<< up to 255-bytes packet with requested data
 * >>> ack/nak
 */
struct __attribute__((__packed__)) tisbl_cmd_mem_read {
	struct tisbl_packet_hdr hdr;
	uint8_t command;
	uint32_t mem_addr;
	uint8_t elem_32bit;
	uint8_t elem_cnt;
};

/* COMMAND_SECTOR_ERASE flow:
 * >>> cmd
 * <<< ack/nak
 * >>> status get
 * <<< ack/nak
 * <<< 3-byte packet with status of last command
 * >>> ack/nak
 */
struct __attribute__((__packed__)) tisbl_cmd_sect_erase {
	struct tisbl_packet_hdr hdr;
	uint8_t command;
	uint32_t sect_addr;
};

#endif
