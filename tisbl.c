/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright (C) 2023 Piotr Dymacz <pepe2k@gmail.com>
 */

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <sys/time.h>
#include <sys/select.h>

#include <libubox/utils.h>

#include "tisbl.h"
#include "umcumgr.h"

#define IMG_CHUNK_LEN	252
#define PACKET_MTU	255

/* Fields offsets in packet */
#define PACKET_LEN_OFF	0
#define PACKET_CHK_OFF	1
#define PACKET_DAT_OFF	2

#define CRC_POLYNOMIAL	0xedb88320

/* Buffers for RX/TX packets */
static uint8_t buf_rx[PACKET_MTU + 1] = { 0 };
static uint8_t buf_tx[PACKET_MTU + 1] = { 0 };

/* Source: netifd/utils.c (OpenWrt project) */
static int
file_crc32(FILE *image, uint32_t *crc32)
{
	uint32_t c, lut[256];
	uint8_t buf[1024];
	size_t i, j, len;

	if (fseek(image, 0, SEEK_SET) < 0)
		return 1;

	for (i = 0; i < 256; ++i) {
		c = i;

		for (j = 0; j < 8; j++)
			c = (c & 1) ? (CRC_POLYNOMIAL ^ (c >> 1)) : (c >> 1);

		lut[i] = c;
	}

	c = 0xffffffff;
	do {
		len = fread(buf, 1, sizeof(buf), image);
		for (i = 0; i < len; i++)
			c = lut[(c ^ buf[i]) & 0xff] ^ (c >> 8);
	} while (len == sizeof(buf));

	*crc32 = c ^ 0xffffffff;

	return 0;
}

static void
status_display(uint8_t status)
{
	char *msg;

	if (quiet)
		return;

	switch (status) {
	case TISBL_STATUS_UNK_CMD:
		msg = "unknown command";
		break;
	case TISBL_STATUS_INV_CMD:
		msg = "invalid command";
		break;
	case TISBL_STATUS_INV_ADR:
		msg = "invalid input address";
		break;
	case TISBL_STATUS_FLASH_FAIL:
		msg = "flash erase/program fail";
		break;
	default:
		msg = "unknown";
		break;
	}

	fprintf(stderr, "command status: %s\n", msg);
}

static uint8_t
packet_checksum(uint8_t *packet)
{
	uint32_t checksum = 0;

	for (int i = 0; i < packet[0] - 2; i++)
		checksum += packet[i + 2];

	/* Checksum is a sum of data, truncated to 8-bit */
	checksum &= 0xff;

	return (uint8_t)checksum;
}

/* 100 ms timeout when checking with dummy command if bootloader is ready */
#define SHORT_TOUT_USEC	100000

static int
packet_rx(bool ack_only, bool short_tout)
{
	struct timeval rx_tout = {timeout, 0};
	int in, sel, pkt_in = 0;
	fd_set set;

	if (short_tout) {
		rx_tout.tv_sec = 0;
		rx_tout.tv_usec = SHORT_TOUT_USEC;
	}

	FD_ZERO(&set);
	FD_SET(uart_fd, &set);

	memset(buf_rx, 0, sizeof(buf_rx));

	while (true) {
		if (short_tout) {
			rx_tout.tv_sec = 0;
			rx_tout.tv_usec = SHORT_TOUT_USEC;
		} else {
			rx_tout.tv_sec = timeout;
			rx_tout.tv_usec = 0;
		}

		sel = select(uart_fd + 1, &set, NULL, NULL, &rx_tout);

		if (sel == 0) {
			if (!short_tout)
				ERR(false, "RX timeout");

			return 1;
		}

		if (sel < 0) {
			ERR(false, "packet RX failed");
			return 1;
		}

		/* Wait for non-zero data (device might send zero bytes between
		 * a sent and a received data packet), first non-zero byte is
		 * the size of the packet */
		if (buf_rx[0] == 0)
			in = read(uart_fd, &buf_rx[0], 1);
		else
			in = read(uart_fd, &buf_rx[pkt_in],
				  PACKET_MTU - pkt_in);

		if (buf_rx[0] == 0)
			continue;

		pkt_in += in;

		/* ACK/NACK is 1-byte only (excluding leading 0x0) */
		if (ack_only) {
			if (pkt_in != 1)
				return 1;

			if (buf_rx[0] == TISBL_RESPONSE_NAK) {
				ERR(false, "NACK received");
				return 1;
			}

			if (buf_rx[0] != TISBL_RESPONSE_ACK)
				return 1;

			return 0;
		} else {
			if (buf_rx[PACKET_LEN_OFF] > PACKET_MTU) {
				ERR(false, "packet too long");
				return 1;
			}

			if (pkt_in == buf_rx[PACKET_LEN_OFF]) {
				if (packet_checksum(buf_rx) !=
				    buf_rx[PACKET_CHK_OFF]) {
					ERR(false, "packet checksum invalid");
					return 1;
				}

				return 0;
			}
		}
	}
}

static int
packet_tx(void)
{
	int out;

	if (buf_tx[PACKET_LEN_OFF] > 3)
		buf_tx[PACKET_CHK_OFF] = packet_checksum(buf_tx);

	out = uart_tx_raw(buf_tx, buf_tx[PACKET_LEN_OFF]);
	if (out != buf_tx[PACKET_LEN_OFF]) {
		ERR(false, "packet send failed");
		return 1;
	}

	return 0;
}

static int
response_tx(bool ack)
{
	if (ack)
		buf_tx[0] = TISBL_RESPONSE_ACK;
	else
		buf_tx[0] = TISBL_RESPONSE_NAK;

	if (uart_tx_raw(buf_tx, 1) != 1)
		return 1;

	return 0;
}

static int
connection_init(void)
{
	buf_tx[0] = TISBL_CMD_DUMMY_LEN;
	buf_tx[1] = TISBL_CMD_DUMMY;
	buf_tx[2] = TISBL_CMD_DUMMY;

	if (packet_tx())
		return 1;

	/* Send baud rate init sequence if bootloader doesn't ACK dummy cmd */
	if (packet_rx(true, true)) {
		buf_tx[0] = 0x55;
		buf_tx[1] = 0x55;

		if (uart_tx_raw(buf_tx, 2) != 2)
			return 1;

		if (packet_rx(true, false))
			return 1;
	}

	return 0;
}

static int
status_get(uint8_t *status)
{
	buf_tx[0] = TISBL_CMD_GET_STATUS_LEN;
	buf_tx[1] = TISBL_CMD_GET_STATUS;
	buf_tx[2] = TISBL_CMD_GET_STATUS;

	if (packet_tx())
		return 1;

	if (packet_rx(true, false))
		return 1;

	if (packet_rx(false, false))
		return 1;

	/* Expect 3-byte packet (size, checksum, status) */
	if (buf_rx[PACKET_LEN_OFF] != 3) {
		response_tx(false);

		ERR(false, "incorrect number of bytes received");
		return 1;
	}

	if (response_tx(true))
		return 1;

	*status = buf_rx[PACKET_DAT_OFF];

	return 0;
}

static int
flash_area_erase(uint32_t off, uint32_t len)
{
	struct tisbl_cmd_sect_erase *packet = (struct tisbl_cmd_sect_erase *)buf_tx;
	uint32_t i, sectors;
	uint8_t status;

	/* Number of 8 KiB sectors to erase */
	sectors = len / CC26XX_FLASH_SECTOR_SIZE;
	if (len % CC26XX_FLASH_SECTOR_SIZE > 0)
		sectors++;

	for (i = 0; i < sectors; i++) {
		packet->hdr.size = TISBL_CMD_SECTOR_ERASE_LEN;
		packet->command = TISBL_CMD_SECTOR_ERASE;
		packet->sect_addr = cpu_to_be32(off);

		if (packet_tx())
			return 1;

		if (packet_rx(true, false))
			return 1;

		if (status_get(&status))
			return 1;

		if (status != TISBL_STATUS_SUCCESS) {
			status_display(status);
			return 1;
		}

		off += CC26XX_FLASH_SECTOR_SIZE;
	}

	return 0;
}

static int
flash_crc32_get(uint32_t off, uint32_t len, uint32_t *crc32)
{
	struct tisbl_cmd_crc32 *packet = (struct tisbl_cmd_crc32 *)buf_tx;
	uint32_t tmp;

	packet->hdr.size = TISBL_CMD_CRC32_LEN;
	packet->command = TISBL_CMD_CRC32;
	packet->data_addr = cpu_to_be32(off);
	packet->data_size = cpu_to_be32(len);
	packet->repeat_cnt = 0;

	if (packet_tx())
		return 1;

	if (packet_rx(true, false))
		return 1;

	if (packet_rx(false, false))
		return 1;

	/* Expect 6-byte packet (size, checksum, 4-byte CRC32 value) */
	if (buf_rx[PACKET_LEN_OFF] != 6) {
		response_tx(false);

		ERR(false, "incorrect number of bytes received");
		return 1;
	}

	if (response_tx(true))
		return 1;

	memcpy(&tmp, &buf_rx[PACKET_DAT_OFF], sizeof(tmp));
	tmp = be32_to_cpu(tmp);

	*crc32 = tmp;

	return 0;
}

static int
flash_size_get(uint32_t *flash_size)
{
	struct tisbl_cmd_mem_read *packet = (struct tisbl_cmd_mem_read *)buf_tx;
	uint32_t tmp;

	packet->hdr.size = TISBL_CMD_MEM_READ_LEN;
	packet->command = TISBL_CMD_MEM_READ;
	packet->elem_32bit = 1;
	packet->elem_cnt = 1;
	packet->mem_addr = cpu_to_be32(CC26XX_FLASH_SIZE_REG);

	if (packet_tx())
		return 1;

	if (packet_rx(true, false))
		return 1;

	if (packet_rx(false, false))
		return 1;

	/* Expect 6-byte packet (size, checksum, 4-byte memory read value) */
	if (buf_rx[PACKET_LEN_OFF] != 6) {
		response_tx(false);

		ERR(false, "incorrect number of bytes received");
		return 1;
	}

	if (response_tx(true))
		return 1;

	memcpy(&tmp, &buf_rx[PACKET_DAT_OFF], sizeof(tmp));
	tmp = le32_to_cpu(tmp);
	tmp &= 0xff;

	*flash_size = tmp * CC26XX_FLASH_SECTOR_SIZE;

	return 0;
}

#if defined(TI_SERIAL_BL_DUMPCMD)
static int
dump(FILE *image, uint32_t off, uint32_t len)
{
	struct tisbl_cmd_mem_read *packet = (struct tisbl_cmd_mem_read *)buf_tx;
	size_t data_in, write_len;

	data_in = 0;

	do {
		packet->hdr.size = TISBL_CMD_MEM_READ_LEN;
		packet->command = TISBL_CMD_MEM_READ;
		packet->elem_32bit = 0;
		packet->mem_addr = cpu_to_be32(off);

		if ((len - data_in) >= IMG_CHUNK_LEN)
			packet->elem_cnt = IMG_CHUNK_LEN;
		else
			packet->elem_cnt = len - data_in;

		if (packet_tx())
			return 1;

		if (packet_rx(true, false))
			return 1;

		if (packet_rx(false, false))
			return 1;

		/* Expect packet->elem_cnt + 2 (size, checksum) bytes */
		if (buf_rx[PACKET_LEN_OFF] != packet->elem_cnt + 2) {
			response_tx(false);

			ERR(false, "incorrect number of bytes received");
			return 1;
		}

		if (response_tx(true))
			return 1;

		write_len = fwrite(&buf_rx[PACKET_DAT_OFF], sizeof(uint8_t),
				   (size_t)(buf_rx[PACKET_LEN_OFF] - 2), image);
		if (write_len != packet->elem_cnt) {
			ERR(false, "firmware write failed");
			return 1;
		}

		data_in += packet->elem_cnt;
		off += packet->elem_cnt;

		if (!quiet) {
			if (data_in != 0)
				fprintf(stdout, "\r");

			fprintf(stdout, "%zu/%d", data_in, len);
			fflush(stdout);
		}
	} while (data_in < len);

	if (!quiet)
		fprintf(stdout, "\n");

	INF("done!");

	return 0;
}
#endif

static int
upload_prepare(uint32_t off, uint32_t len, uint32_t crc32)
{
	struct tisbl_cmd_dl_crc *packet = (struct tisbl_cmd_dl_crc *)buf_tx;
	uint8_t status;

	packet->hdr.size = TISBL_CMD_DOWNLOAD_CRC_LEN;
	packet->command = TISBL_CMD_DOWNLOAD_CRC;
	packet->data_addr = cpu_to_be32(off);
	packet->data_size = cpu_to_be32(len);
	packet->crc32 = cpu_to_be32(crc32);

	if (packet_tx())
		return 1;

	if (packet_rx(true, false))
		return 1;

	if (status_get(&status))
		return 1;

	if (status != TISBL_STATUS_SUCCESS) {
		status_display(status);
		return 1;
	}

	return 0;
}

static int
upload(FILE *image, uint32_t image_len)
{
	struct tisbl_cmd_simple *packet = (struct tisbl_cmd_simple *)buf_tx;
	size_t data_out, read_len;
	uint8_t status;

	rewind(image);
	data_out = 0;

	do {
		read_len = fread(&buf_tx[3], 1, IMG_CHUNK_LEN, image);
		if (read_len == 0) {
			ERR(false, "firmware read failed");
			return 1;
		}

		packet->hdr.size = sizeof(struct tisbl_cmd_simple) + read_len;
		packet->command = TISBL_CMD_SEND_DATA;

		if (packet_tx())
			return 1;

		if (packet_rx(true, false))
			return 1;

		if (status_get(&status))
			return 1;

		if (status != TISBL_STATUS_SUCCESS) {
			status_display(status);
			return 1;
		}

		data_out += read_len;

		if (!quiet) {
			if (data_out != 0)
				fprintf(stdout, "\r");

			fprintf(stdout, "%zu/%d", data_out, image_len);
			fflush(stdout);
		}
	} while (data_out < image_len);

	if (!quiet)
		fprintf(stdout, "\n");

	INF("done!");

	return 0;
}

#if defined(TI_SERIAL_BL_DUMPCMD)
int
tisbl_dump(FILE *image)
{
	uint32_t img_crc32, mcu_crc32, mcu_flash;

	if (connection_init())
		return 1;

	if (flash_size_get(&mcu_flash))
		return 1;

	if (flash_crc32_get(CC26XX_FLASH_START_ADDR, mcu_flash, &mcu_crc32))
		return 1;

	if (dump(image, CC26XX_FLASH_START_ADDR, mcu_flash))
		return 1;

	fflush(image);

	if (file_crc32(image, &img_crc32))
		return 1;

	/* Make sure downloaded image has the same checksum */
	if (img_crc32 != mcu_crc32) {
		ERR(false, "CRC32 mismatch, dump failed");
		return 1;
	}

	return 0;
}
#endif

int
tisbl_reset(void)
{
	buf_tx[0] = TISBL_CMD_RESET_LEN;
	buf_tx[1] = TISBL_CMD_RESET;
	buf_tx[2] = TISBL_CMD_RESET;

	if (connection_init())
		return 1;

	if (packet_tx())
		return 1;

	if (packet_rx(true, false))
		return 1;

	return 0;
}

int
tisbl_upload(FILE *image, int image_len)
{
	uint32_t img_crc32, mcu_crc32, mcu_flash;

	if (file_crc32(image, &img_crc32))
		return 1;

	if (connection_init())
		return 1;

	/* Make sure image will fit in MCU's flash */
	if (flash_size_get(&mcu_flash))
		return 1;

	if (image_len > mcu_flash) {
		ERR(false, "image too big");
		return 1;
	}

	/* Compare CRC32 checksums of the image and target MCU's flash area */
	if (flash_crc32_get(CC26XX_FLASH_START_ADDR, image_len, &mcu_crc32))
		return 1;

	if (img_crc32 == mcu_crc32) {
		INF("CRC32 matches, same firmware is already on the device");
		return 0;
	}

	/* Erase target MCU's flash */
	if (flash_area_erase(CC26XX_FLASH_START_ADDR, image_len))
		return 1;

	if (upload_prepare(CC26XX_FLASH_START_ADDR, image_len, img_crc32))
		return 1;

	if (upload(image, image_len))
		return 1;

	return 0;
}
