/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright (C) 2022 Piotr Dymacz <pepe2k@gmail.com>
 */

#ifndef __UMCUMGR_H
#define __UMCUMGR_H

#include <stdint.h>
#include <termios.h>

/* Sane defaults */
#define OPT_DEFAULT_BAUD	115200
#define OPT_DEFAULT_PORT	"/dev/ttyACM0"
#define OPT_DEFAULT_TOUT	3
#define OPT_DEFAULT_HWFC	false
#define OPT_DEFAULT_SLOT	0

#define OPT_MAX_ECHOLEN		128

extern int timeout;
extern int uart_fd;
extern bool simple;
extern bool quiet;
#if defined(TI_SERIAL_BL_SUPPORT)
extern bool ti_sbl;
#endif
void usage(bool error);

int uart_tx_raw(uint8_t *buf, int len);
int port_setup(int baudrate, bool hw_flow, struct termios *tio_backup);

/* Program commands list */
enum {
	CMD_ACK = 0x01,
	CMD_BOO = 0x02,
	CMD_DMP = 0x04,
	CMD_ECH = 0x08,
	CMD_INF = 0x10,
	CMD_LST = 0x20,
	CMD_RST = 0x40,
	CMD_SEL = 0x80,
	CMD_SHA = 0x100,
	CMD_SYS = 0x200,
	CMD_TST = 0x400,
	CMD_UPL = 0x800,
};

/* ERR and INF print helpers */
#define ERR(print_usage, format, ...)						\
	do {									\
		if (!quiet) {							\
			fprintf(stderr, "ERROR: " format "\n", ## __VA_ARGS__);	\
										\
			if (print_usage) {					\
				fprintf(stderr, "\n");				\
				usage(true);					\
			}							\
		}								\
	} while (0)

#define INF(format, ...)							\
	do {									\
		if (!quiet)							\
			fprintf(stdout, "INF: " format "\n", ## __VA_ARGS__);	\
	} while (0)

#endif
