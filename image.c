/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright (C) 2022 Piotr Dymacz <pepe2k@gmail.com>
 */

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "image.h"
#include "umcumgr.h"

static const char *image_flags[] = {
	"pic",				/* 0x1   */
	"undef",			/* 0x2   */
	"aes128",			/* 0x4   */
	"aes256",			/* 0x8   */
	"non-bootable",			/* 0x10  */
	"ram-load-addr-in-hdr",		/* 0x20  */
	"undef",			/* 0x40  */
	"undef",			/* 0x80  */
	"rom-load-addr-in-hdr"		/* 0x100 */
};

static const char *
image_info_tlvtype_simple(uint16_t type)
{
	static char buf[32] = { 0 };

	switch (type) {
	case IMAGE_TLV_SHA256:
		return "hash";
		break;
	case IMAGE_TLV_OW_FW_METADATA:
		return "metadata";
		break;
	default:
		snprintf(buf, sizeof(buf), "tlv_%x=", type);
		return buf;
		break;
	}
}

static const char *
image_info_tlvtype(uint16_t type)
{
	switch (type) {
	case IMAGE_TLV_KEYHASH:
		return "public key hash";
		break;
	case IMAGE_TLV_PUBKEY:
		return "public key";
		break;
	case IMAGE_TLV_SHA256:
		return "SHA256 of image and header";
		break;
	case IMAGE_TLV_RSA2048_PSS:
		return "RSA2048 of hash output";
		break;
	case IMAGE_TLV_ECDSA224:
		return "ECDSA224 of hash output";
		break;
	case IMAGE_TLV_ECDSA256:
		return "ECDSA256 of hash output";
		break;
	case IMAGE_TLV_RSA3072_PSS:
		return "RSA3072 of hash output";
		break;
	case IMAGE_TLV_ED25519:
		return "ED25519 of hash output";
		break;
	case IMAGE_TLV_ENC_RSA2048:
		return "key encrypted with RSA-OAEP-2048";
		break;
	case IMAGE_TLV_ENC_KW:
		return "key encrypted with AES-KW 128 or 256";
		break;
	case IMAGE_TLV_ENC_EC256:
		return "key encrypted with ECIES-EC256";
		break;
	case IMAGE_TLV_ENC_X25519:
		return "key encrypted with ECIES-X25519";
		break;
	case IMAGE_TLV_DEPENDENCY:
		return "image dependency information";
		break;
	case IMAGE_TLV_SEC_CNT:
		return "security counter";
		break;
	case IMAGE_TLV_BOOT_RECORD:
		return "measured boot record";
		break;
	case IMAGE_TLV_OW_FW_METADATA:
		return "firmware metadata (JSON)";
		break;
	default:
		return "unknown";
		break;
	}
}

static void
image_info_flags(uint32_t flags)
{
	int i, found;
	size_t len;

	len = sizeof(image_flags) / sizeof(char *);

	if (flags > 0) {
		found = 0;
		fprintf(stdout, "[");
		for (i = 0; i < len; i++) {
			if (flags & (1 << i)) {
				if (found)
					fprintf(stdout, " | ");

				fprintf(stdout, "%s (0x%x)", image_flags[i], i);
				found++;
			}
		}
		fprintf(stdout, "]\n");

	} else
		fprintf(stdout, "[none]\n");
}

int
image_sha256(FILE *image, int image_len, char *buf, size_t buf_len)
{
	struct image_tlv_info img_tlr = { 0 };
	struct image_header img_hdr = { 0 };
	struct image_tlv img_tlv = { 0 };
	uint16_t sha_len;
	size_t len, ofs;

	rewind(image);

	len = fread(&img_hdr, 1, sizeof(img_hdr), image);
	if (len != sizeof(img_hdr))
		return 0;

	ofs = le16_to_cpu(img_hdr.ih_hdr_size) +
	      le32_to_cpu(img_hdr.ih_img_size);

	if (le16_to_cpu(img_hdr.ih_protect_tlv_size) > 0)
		ofs += le16_to_cpu(img_hdr.ih_protect_tlv_size);

	/* No trailer */
	if (ofs == image_len)
		return 0;

	if (fseek(image, ofs, SEEK_SET) < 0)
		return 0;

	len = fread(&img_tlr, 1, sizeof(img_tlr), image);
	if (len != sizeof(img_tlr))
		return 0;

	while (ftell(image) < image_len) {
		len = fread(&img_tlv, 1, sizeof(img_tlv), image);
		if (len != sizeof(img_tlv))
			return 0;

		sha_len = le16_to_cpu(img_tlv.it_len);

		if (le16_to_cpu(img_tlv.it_type) == IMAGE_TLV_SHA256) {
			if (sha_len == 0 || sha_len > buf_len)
				return 0;

			len = fread(buf, 1, sha_len, image);
			if (len != sha_len)
				return 0;

			return (int)sha_len;
		}

		if (fseek(image, sha_len, SEEK_CUR) < 0)
			return 0;
	}

	return 0;
}

int
image_info(FILE *image, int image_len)
{
	struct image_tlv_info img_tlr = { 0 };
	struct image_header img_hdr = { 0 };
	struct image_tlv img_tlv = { 0 };
	size_t len, ofs, tlv_len;
	bool protected;
	uint8_t data;
	int i, j;

	rewind(image);

	len = fread(&img_hdr, 1, sizeof(img_hdr), image);
	if (len != sizeof(img_hdr)) {
		ERR(false, "firmware header read failed");
		return 1;
	}

	if (simple) {
		fprintf(stdout,
			"ih_magic=0x%x\n"
			"ih_load_addr=0x%x\n"
			"ih_hdr_size=%u\n"
			"ih_protect_tlv_size=%u\n"
			"ih_img_size=%u\n"
			"ih_flags=0x%x\n",
			le32_to_cpu(img_hdr.ih_magic),
			le32_to_cpu(img_hdr.ih_load_addr),
			le16_to_cpu(img_hdr.ih_hdr_size),
			le16_to_cpu(img_hdr.ih_protect_tlv_size),
			le32_to_cpu(img_hdr.ih_img_size),
			le32_to_cpu(img_hdr.ih_flags)
		);
	} else {
		fprintf(stdout,
			"[Image header]\n"
			"  magic             = 0x%08x [%s]\n"
			"  load_addr         = 0x%08x\n"
			"  hdr_size          = %u\n"
			"  protect_tlv_size  = %u\n"
			"  img_size          = %u\n"
			"  flags             = 0x%08x ",
			le32_to_cpu(img_hdr.ih_magic),
			le32_to_cpu(img_hdr.ih_magic) == IMAGE_MAGIC ?
							"valid" : "invalid",
			le32_to_cpu(img_hdr.ih_load_addr),
			le16_to_cpu(img_hdr.ih_hdr_size),
			le16_to_cpu(img_hdr.ih_protect_tlv_size),
			le32_to_cpu(img_hdr.ih_img_size),
			le32_to_cpu(img_hdr.ih_flags)
		);

		image_info_flags(le32_to_cpu(img_hdr.ih_flags));

		fprintf(stdout,
			"  version           = %u.%u.%u-%u\n",
			img_hdr.ih_ver.iv_major,
			img_hdr.ih_ver.iv_minor,
			img_hdr.ih_ver.iv_revision,
			img_hdr.ih_ver.iv_build_num
		);
	}

	ofs = le16_to_cpu(img_hdr.ih_hdr_size) +
	      le32_to_cpu(img_hdr.ih_img_size);

	/* No trailer */
	if (ofs == image_len)
		return 0;

	if (fseek(image, ofs, SEEK_SET) < 0) {
		ERR(false, "fseek on firmware failed");
		return 1;
	}

img_tlv_read:
	protected = false;

	len = fread(&img_tlr, 1, sizeof(img_tlr), image);
	if (len != sizeof(img_tlr)) {
		ERR(false, "firmware trailer read failed");
		return 1;
	}

	if (le16_to_cpu(img_tlr.it_magic) != IMAGE_TLV_PROT_INFO_MAGIC &&
	    le16_to_cpu(img_tlr.it_magic) != IMAGE_TLV_INFO_MAGIC) {
		ERR(false, "unknown TLV header info magic");
		return 1;
	}

	if (le16_to_cpu(img_tlr.it_magic) == IMAGE_TLV_PROT_INFO_MAGIC)
		protected = true;

	if (!simple)
		fprintf(stdout,
			"\n"
			"[%s image trailer info header]\n"
			"  magic    = 0x%04x [%s]\n"
			"  tlv_tot  = %u\n",
			protected ? "Protected" : "Regular",
			le16_to_cpu(img_tlr.it_magic),
			le16_to_cpu(img_tlr.it_magic) == IMAGE_TLV_INFO_MAGIC ||
			le16_to_cpu(img_tlr.it_magic) == IMAGE_TLV_PROT_INFO_MAGIC ?
							"valid" : "invalid",
			le16_to_cpu(img_tlr.it_tlv_tot)
		);

	tlv_len = le16_to_cpu(img_tlr.it_tlv_tot) - sizeof(img_tlr);

	i = 0;
	while (ftell(image) < image_len) {
		len = fread(&img_tlv, 1, sizeof(img_tlv), image);
		if (len != sizeof(img_tlv)) {
			ERR(false, "firmware trailer TLV read failed");
			return 1;
		}

		if (simple) {
			fprintf(stdout, "%s=",
				image_info_tlvtype_simple(le16_to_cpu(img_tlv.it_type)));

			for (j = 0; j < le16_to_cpu(img_tlv.it_len); j++) {
				len = fread(&data, 1, 1, image);
				if (len == 1) {
					if (le16_to_cpu(img_tlv.it_type) ==
					    IMAGE_TLV_OW_FW_METADATA)
						fprintf(stdout, "%c", (char)data);
					else
						fprintf(stdout, "%02x", data);
				}
			}
		} else {
			fprintf(stdout,
				"\n"
				"[%s image trailer TLV #%d]\n"
				"  type = 0x%04x [%s]\n"
				"  len  = %u\n",
				protected ? "Protected" : "Regular",
				i,
				le16_to_cpu(img_tlv.it_type),
				image_info_tlvtype(le16_to_cpu(img_tlv.it_type)),
				le16_to_cpu(img_tlv.it_len)
			);

			fprintf(stdout, "  data = ");
			for (j = 0; j < le16_to_cpu(img_tlv.it_len); j++) {
				len = fread(&data, 1, 1, image);
				if (len == 1) {
					if (j && j % 16 == 0)
						fprintf(stdout, "\n         ");

					fprintf(stdout, "%02x ", data);
				}
			}
		}
		fprintf(stdout, "\n");

		tlv_len -= (le16_to_cpu(img_tlv.it_len) + sizeof(img_tlv));
		if (tlv_len == 0)
			break;

		i++;
	}

	if (ftell(image) < image_len)
		goto img_tlv_read;

	return 0;
}
