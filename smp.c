/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright (C) 2022 Piotr Dymacz <pepe2k@gmail.com>
 */

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <limits.h>

#include <sys/time.h>
#include <sys/select.h>

#include <libubox/utils.h>

#include "image.h"
#include "smp.h"
#include "umcumgr.h"

#include "zcbor/zcbor_encode.h"
#include "zcbor_bulk.h"

#define IMG_CHUNK_LEN	256
#define FRAME_BUF_LEN	768
#define FRAME_MTU	128
#define CBOR_DATA_OFF	(2 + MGMT_HDR_SIZE) /* 2 bytes for SMP packet len */
#define INT_FRAME_SLEEP	(20 * 1000)

/* For use in CBOR maps */
#if defined(CONFIRM_TEST_SUPPORT)
static const char tstr_confirm[] = "confirm";
static const char tstr_hash[] = "hash";
#endif
static const char tstr_slot[] = "slot";
static const char tstr_img[] = "image";
static const char tstr_dat[] = "data";
static const char tstr_len[] = "len";
static const char tstr_sha[] = "sha";
static const char tstr_off[] = "off";
static const char tstr_d[] = "d";

/* Buffers for UART tx/rx and raw SMP packet */
static uint8_t buf_trx[FRAME_BUF_LEN] = { 0 };
static uint8_t buf_smp[FRAME_BUF_LEN] = { 0 };

static uint8_t buf_frame[FRAME_MTU] = { 0 };

/* Management header always starts after 2-byte packet length */
static struct mgmt_hdr *hdr = (struct mgmt_hdr *)(&buf_smp[2]);

static uint8_t smp_seq = 0;

static uint16_t
crc16_xmodem(uint8_t *buf, size_t len) {
	uint32_t crc = 0;
	uint8_t b;
	int i, j;

	for (i = 0; i < len; i++) {
		b = buf[i];
		crc = crc ^ (b << 8);

		for (j = 0; j < 8; j++) {
			crc = crc << 1;
			if (crc & 0x10000)
				crc = (crc ^ 0x1021) & 0xffff;
		}
	}

	return (uint16_t)crc;
}

static void
mgmt_status_display(int rc)
{
	char *msg;

	if (quiet)
		return;

	switch (rc) {
	case MGMT_ERR_EOK:
		msg = "no error";
		break;
	case MGMT_ERR_EUNKNOWN:
		msg = "unknown error";
		break;
	case MGMT_ERR_ENOMEM:
		msg = "not enough memory";
		break;
	case MGMT_ERR_EINVAL:
		msg = "invalid value";
		break;
	case MGMT_ERR_ETIMEOUT:
		msg = "timeout";
		break;
	case MGMT_ERR_ENOENT:
		msg = "no entry";
		break;
	case MGMT_ERR_EBADSTATE:
		msg = "bad state";
		break;
	case MGMT_ERR_EMSGSIZE:
		msg = "response too long";
		break;
	case MGMT_ERR_ENOTSUP:
		msg = "not supported";
		break;
	case MGMT_ERR_ECORRUPT:
		msg = "corrupted payload received";
		break;
	case MGMT_ERR_EBUSY:
		msg = "device is busy";
		break;
	case MGMT_ERR_EPERUSER:
	default:
		msg = "unknown";
		break;
	}

	fprintf(stderr, "MGMT error (%d): %s\n", rc, msg);
}

static int
mgmt_header_chk(uint8_t op, uint16_t grp, uint8_t id)
{
	/* We should at least receive minimal response
	 * (header + CBOR map container which might be empty) */
	if (be16_to_cpu(hdr->nh_len) == 0) {
		ERR(false, "response without CBOR");
		return 1;
	}

	if ((op == MGMT_OP_WRITE && hdr->nh_op != MGMT_OP_WRITE_RSP) ||
	    (op == MGMT_OP_READ && hdr->nh_op != MGMT_OP_READ_RSP) ||
	    hdr->nh_id != id || hdr->nh_seq != smp_seq ||
	    be16_to_cpu(hdr->nh_group) != grp) {
		ERR(false, "response for different request");
		return 1;
	}

	return 0;
}

static int
rx_smp_frame()
{
	struct timeval rx_tout = {timeout, 0};
	uint16_t buf, crc16, smp_len = 0;
	int b64_len, in, sel;
	int smp_in = 0;
	fd_set set;

	FD_ZERO(&set);
	FD_SET(uart_fd, &set);

	while (true) {
		memset(buf_frame, 0, FRAME_MTU);

		rx_tout.tv_sec = timeout;
		rx_tout.tv_usec = 0;

		sel = select(uart_fd + 1, &set, NULL, NULL, &rx_tout);

		if (sel == 0) {
			ERR(false, "RX timeout");
			return 1;
		}

		if (sel < 0) {
			ERR(false, "response RX failed");
			return 1;
		}

		in = read(uart_fd, buf_frame, FRAME_MTU - 1);

		if ((buf_frame[in - 1] != 0xa) ||
		    ((buf_frame[0] != 0x06 && buf_frame[1] != 0x09) &&
		     (buf_frame[0] != 0x04 && buf_frame[1] != 0x14))) {
			ERR(false, "received incomplete frame");
			return 1;
		}

		/* Is there still place for more data in SMP buffer? */
		if (B64_DECODE_LEN(in - 3) > FRAME_BUF_LEN - smp_in) {
			ERR(false, "response too long");
			return 1;
		}

		/* Payload base64 decode (exclude frame indicator and end) */
		b64_len = b64_decode(&buf_frame[2], &buf_smp[smp_in], in - 3);
		if (b64_len < 0) {
			ERR(false, "base64 decode failed");
			return 1;
		}

		/* First frame? Check length of SMP packet */
		if (smp_in == 0) {
			memcpy(&buf, buf_smp, 2);
			smp_len = be16_to_cpu(buf);

			/* Include 2 bytes of length field in SMP length */
			smp_len += 2;
		}

		smp_in += b64_len;

		/* Whole SMP frame collected? */
		if (smp_in > 0 && smp_in == smp_len)
			break;
	}

	/* Subtract 2-byte CRC */
	smp_len -= 2;

	memcpy(&buf, &buf_smp[smp_len], 2);
	crc16 = be16_to_cpu(buf);

	/* Subtract 2-byte SMP length field */
	smp_len -= 2;

	/* Validate CRC16 */
	if (crc16 != crc16_xmodem(&buf_smp[2], (size_t)(smp_len))) {
		ERR(false, "invalid CRC16");
		return 1;
	}

	return 0;
}

static int
tx_smp_frame(int frame_len)
{
	uint8_t frame_init[] = { 0x06, 0x09 };
	uint8_t frame_cont[] = { 0x04, 0x14 };
	uint8_t frame_stop[] = { 0x0a };
	int b64_len, to_out, out;
	int sent_len = 0;

	/* Apply base64 encoding on the whole SMP packet */
	b64_len = b64_encode(buf_smp, frame_len, buf_trx, FRAME_BUF_LEN);

	if (b64_len < 0) {
		ERR(false, "base64 encode failed");
		return 1;
	}

	do {
		/* First frame? */
		if (sent_len == 0) {
			out = uart_tx_raw(frame_init, 2);
		} else {
			/* Give MCU some time to handle previous frame/s */
			usleep(INT_FRAME_SLEEP);
			out = uart_tx_raw(frame_cont, 2);
		}

		if (out != 2) {
			ERR(false, "frame start TX failed");
			return 1;
		}

		/* Send up to 127 bytes total at once (inc. frame start/stop) */
		to_out = MIN(124, b64_len - sent_len);
		out = uart_tx_raw(&buf_trx[sent_len], to_out);
		if (out != to_out) {
			ERR(false, "frame payload TX failed");
			return 1;
		}

		/* Frame stop */
		out = uart_tx_raw(frame_stop, 1);
		if (out != 1) {
			ERR(false, "frame stop TX failed");
			return 1;
		}

		sent_len += to_out;
	} while (sent_len < b64_len);

	return 0;
}

static int
images_state_display()
{
	bool rsp_active, rsp_bootable, rsp_confirmed, rsp_pending, rsp_permanent;
	struct smp_imglist_state imglist[IMAGE_SLOTS_CNT_MAX] = { 0 };
	struct zcbor_string rsp_hash, rsp_metadata, rsp_version;
	uint32_t slots_cnt, rsp_image, rsp_slot;
	struct zcbor_string rsp_text = { 0 };
	size_t cbor_len, decoded;
	int rsp_rc;
	bool ok;

	struct zcbor_map_decode_key_val list_map[] = {
		ZCBOR_MAP_DECODE_KEY_DECODER("image", zcbor_uint32_decode,
					     &rsp_image),
		ZCBOR_MAP_DECODE_KEY_DECODER("slot", zcbor_uint32_decode,
					     &rsp_slot),
		ZCBOR_MAP_DECODE_KEY_DECODER("version", zcbor_tstr_decode,
					     &rsp_version),
		ZCBOR_MAP_DECODE_KEY_DECODER("hash", zcbor_bstr_decode,
					     &rsp_hash),
		ZCBOR_MAP_DECODE_KEY_DECODER("metadata", zcbor_bstr_decode,
					     &rsp_metadata),
		ZCBOR_MAP_DECODE_KEY_DECODER("active", zcbor_bool_decode,
					     &rsp_active),
		ZCBOR_MAP_DECODE_KEY_DECODER("bootable", zcbor_bool_decode,
					     &rsp_bootable),
		ZCBOR_MAP_DECODE_KEY_DECODER("confirmed", zcbor_bool_decode,
					     &rsp_confirmed),
		ZCBOR_MAP_DECODE_KEY_DECODER("pending", zcbor_bool_decode,
					     &rsp_pending),
		ZCBOR_MAP_DECODE_KEY_DECODER("permanent", zcbor_bool_decode,
					     &rsp_permanent)
		};

	cbor_len = (size_t)(be16_to_cpu(hdr->nh_len));

	ZCBOR_STATE_D(zsd, 3, &buf_smp[CBOR_DATA_OFF], cbor_len, 1, 0);

	ok = zcbor_map_start_decode(zsd) &&
	     zcbor_tstr_decode(zsd, &rsp_text);

	if (!ok)
		goto decode_fail;

	/* Different CBOR data in case of an error */
	if (rsp_text.len == sizeof("rc") - 1 &&
	    !memcmp(rsp_text.value, "rc", sizeof("rc") - 1)) {
		ok = zcbor_int32_decode(zsd, &rsp_rc);

		if (!ok)
			goto decode_fail;

		mgmt_status_display(rsp_rc);
		return 1;
	}

	if (rsp_text.len != sizeof("images") - 1 ||
	    memcmp(rsp_text.value, "images", sizeof("images") - 1))
		goto decode_fail;

	ok = zcbor_list_start_decode(zsd);
	if (!ok)
		goto decode_fail;

	slots_cnt = 0;
	while (slots_cnt < IMAGE_SLOTS_CNT_MAX) {
		decoded = 0;
		rsp_image = UINT32_MAX;
		rsp_slot = UINT32_MAX;

		rsp_version.len = 0;
		rsp_hash.len = 0;
		rsp_metadata.len = 0;

		rsp_active = false;
		rsp_bootable = false;
		rsp_confirmed = false;
		rsp_pending = false;
		rsp_permanent = false;

		zcbor_map_decode_bulk_reset(list_map, ARRAY_SIZE(list_map));

		ok = zcbor_map_decode_bulk(zsd, list_map, ARRAY_SIZE(list_map),
					   &decoded) == 0;

		if (!ok)
			break;

		if (rsp_hash.len != IMAGE_SHA256_SIZE || rsp_version.len == 0 ||
		    rsp_slot == UINT32_MAX) {
			ERR(false, "Missing or incorrect mandatory fields");
			return 1;
		}

		imglist[slots_cnt].slot = rsp_slot;
		imglist[slots_cnt].image = rsp_image;

		/* Hash, version and metadata */
		memcpy(imglist[slots_cnt].hash, rsp_hash.value,
		       IMAGE_SHA256_SIZE);

		if (rsp_version.len > IMAGE_VERSION_SIZE)
			rsp_version.len = IMAGE_VERSION_SIZE;

		memcpy(imglist[slots_cnt].version, rsp_version.value,
		       rsp_version.len);

		imglist[slots_cnt].version[rsp_version.len] = '\0';

		if (rsp_metadata.len) {
			if (rsp_metadata.len > IMAGE_METADATA_SIZE)
				rsp_metadata.len = IMAGE_METADATA_SIZE;

			memcpy(imglist[slots_cnt].metadata, rsp_metadata.value,
			       rsp_metadata.len);

			imglist[slots_cnt].metadata[rsp_metadata.len] = '\0';
		}

		/* Flags */
		imglist[slots_cnt].flags.active = rsp_active;
		imglist[slots_cnt].flags.bootable = rsp_bootable;
		imglist[slots_cnt].flags.confirmed = rsp_confirmed;
		imglist[slots_cnt].flags.pending = rsp_pending;
		imglist[slots_cnt].flags.permanent = rsp_permanent;

		slots_cnt++;
	}

	ok = zcbor_list_end_decode(zsd);
	if (!ok)
		goto decode_fail;

	/* No images (empty list) */
	if (slots_cnt == 0)
		return 0;

	/* Print image list information */
	if (simple)
		fprintf(stdout, "images=%d\n", slots_cnt);
	else
		fprintf(stdout, "Images:\n");

	for (int i = 0; i < slots_cnt; i++) {
		if (!simple) {
			fprintf(stdout, " image=%d",
				imglist[i].image == UINT32_MAX ? 0 :
				imglist[i].image);

			fprintf(stdout, " slot=%d\n", imglist[i].slot);
		}

		if (simple)
			fprintf(stdout, "slot%d_version=%s\n",
				imglist[i].slot, imglist[i].version);
		else
			fprintf(stdout, "    version: %s\n", imglist[i].version);

		if (simple)
			fprintf(stdout, "slot%d_bootable=", imglist[i].slot);
		else
			fprintf(stdout, "    bootable: ");

		if (imglist[i].flags.bootable) {
			if (simple)
				fprintf(stdout, "1");
			else
				fprintf(stdout, "true");
		} else {
			if (simple)
				fprintf(stdout, "0");
			else
				fprintf(stdout, "false");
		}

		fprintf(stdout, "\n");

		if (!simple)
			fprintf(stdout, "    flags:");

		if (imglist[i].flags.active) {
			if (simple)
				fprintf(stdout, "slot%d_active=1\n",
					imglist[i].slot);
			else
				fprintf(stdout, " active");
		}

		if (imglist[i].flags.pending) {
			if (simple)
				fprintf(stdout, "slot%d_pending=1\n",
					imglist[i].slot);
			else
				fprintf(stdout, " pending");
		}

		if (imglist[i].flags.confirmed) {
			if (simple)
				fprintf(stdout, "slot%d_confirmed=1\n",
					imglist[i].slot);
			else
				fprintf(stdout, " confirmed");
		}

		if (imglist[i].flags.permanent) {
			if (simple)
				fprintf(stdout, "slot%d_permanent=1\n",
					imglist[i].slot);
			else
				fprintf(stdout, " permanent");
		}

		if (!simple)
			fprintf(stdout, "\n");

		if (simple)
			fprintf(stdout, "slot%d_hash=", imglist[i].slot);
		else
			fprintf(stdout, "    hash: ");

		for (int j = 0; j < IMAGE_SHA256_SIZE; j++)
			fprintf(stdout, "%02x", imglist[i].hash[j]);

		fprintf(stdout, "\n");

		if (simple)
			fprintf(stdout, "slot%d_metadata=", imglist[i].slot);
		else
			fprintf(stdout, "    metadata: ");

		if (imglist[i].metadata[0] != '\0') {
			fprintf(stdout, "%s", imglist[i].metadata);
		} else if (!simple)
			fprintf(stdout, "unavailable");

		fprintf(stdout, "\n");
	}

	return 0;

decode_fail:
	ERR(false, "CBOR decode error: %d", zcbor_peek_error(zsd));
	return 1;
}

static uint8_t *
smp_setup(uint8_t mgmt_op, uint16_t mgmt_grp, uint8_t mgmt_id,
	  zcbor_state_t *zs)
{
	uint8_t *buf_offset = &buf_smp[CBOR_DATA_OFF];
	uint16_t *ptr;
	int cbor_len;

	smp_seq == 255 ? smp_seq = 0 : smp_seq++;

	/* SMP payload len (CBOR data) */
	cbor_len = (int)(zs->payload_mut - buf_offset);
	buf_offset += cbor_len;

	/* Fill MGMT header */
	hdr->nh_op    = mgmt_op;
	hdr->_res1    = 0;
	hdr->nh_flags = 0;
	hdr->nh_len   = cpu_to_be16(cbor_len);
	hdr->nh_group = cpu_to_be16(mgmt_grp);
	hdr->nh_seq   = smp_seq;
	hdr->nh_id    = mgmt_id;

	/* Calculate 16-bit CRC of the whole SMP packet */
	ptr = (uint16_t *)buf_offset;
	*ptr = cpu_to_be16(crc16_xmodem(&buf_smp[2], MGMT_HDR_SIZE + cbor_len));
	buf_offset += 2;

	/* First 2 bytes: total size of SMP packet (inc. 2 byte CRC) */
	ptr = (uint16_t *)buf_smp;
	*ptr = cpu_to_be16(buf_offset - &buf_smp[2]);

	return buf_offset;
}

int
smp_mgmt_list()
{
	uint8_t *buf_end;
	bool ok;

	ZCBOR_STATE_E(zse, 0, &buf_smp[CBOR_DATA_OFF],
		      FRAME_BUF_LEN - CBOR_DATA_OFF, 0);

	ok = zcbor_map_start_encode(zse, 1) &&
	     zcbor_map_end_encode(zse, 1);

	if (!ok) {
		ERR(false, "CBOR encode error: %d", zcbor_peek_error(zse));
		return 1;
	}

	buf_end = smp_setup(MGMT_OP_READ, MGMT_GROUP_ID_IMAGE,
			    MGMT_IMG_CMD_ID_LIST, zse);

	if (tx_smp_frame(buf_end - buf_smp) != 0)
		return 1;

	if (rx_smp_frame() != 0)
		return 1;

	if (mgmt_header_chk(MGMT_OP_READ,
			    MGMT_GROUP_ID_IMAGE,
			    MGMT_IMG_CMD_ID_LIST) != 0)
		return 1;

	return images_state_display();
}

#if defined(CONFIRM_TEST_SUPPORT)
int
smp_mgmt_confirm(const char *hash, bool confirm)
{
	char image_sha[IMAGE_SHA256_SIZE] = { 0 };
	uint8_t *buf_end;
	bool ok;

	ZCBOR_STATE_E(zse, 1, &buf_smp[CBOR_DATA_OFF],
		      FRAME_BUF_LEN - CBOR_DATA_OFF, 0);

	ok = zcbor_map_start_encode(zse, 10)       &&
	     zcbor_tstr_put_lit(zse, tstr_confirm) &&
	     zcbor_bool_put(zse, confirm);

	if (hash) {
		for (int i = 0, j = 0; i < IMAGE_SHA256_SIZE; i++, j+=2) {
			if (sscanf(&hash[j], "%02hhx", &image_sha[i]) != 1) {
				ERR(false, "invalid hash value");
				return 1;
			}
		}

		ok &= zcbor_tstr_put_lit(zse, tstr_hash) &&
		      zcbor_bstr_encode_ptr(zse, image_sha, IMAGE_SHA256_SIZE);
	}

	ok &= zcbor_map_end_encode(zse, 10);

	if (!ok) {
		ERR(false, "CBOR encode error: %d", zcbor_peek_error(zse));
		return 1;
	}

	buf_end = smp_setup(MGMT_OP_WRITE, MGMT_GROUP_ID_IMAGE,
			    MGMT_IMG_CMD_ID_LIST, zse);

	if (tx_smp_frame(buf_end - buf_smp) != 0)
		return 1;

	if (rx_smp_frame() != 0)
		return 1;

	if (mgmt_header_chk(MGMT_OP_WRITE,
			    MGMT_GROUP_ID_IMAGE,
			    MGMT_IMG_CMD_ID_LIST) != 0)
		return 1;

	return images_state_display();
}
#endif

int
smp_mgmt_echo(const char *text)
{
	struct zcbor_string rsp_r = { 0 };
	size_t cbor_len, decoded;
	int rsp_rc = INT_MAX;
	uint8_t *buf_end;
	bool ok;

	ZCBOR_STATE_E(zse, 0, &buf_smp[CBOR_DATA_OFF],
		      FRAME_BUF_LEN - CBOR_DATA_OFF, 0);

	ok = zcbor_map_start_encode(zse, 10)                &&
	     zcbor_tstr_put_lit(zse, tstr_d)                &&
	     zcbor_tstr_encode_ptr(zse, text, strlen(text)) &&
	     zcbor_map_end_encode(zse, 10);

	if (!ok) {
		ERR(false, "CBOR encode error: %d", zcbor_peek_error(zse));
		return 1;
	}

	buf_end = smp_setup(MGMT_OP_WRITE, MGMT_GROUP_ID_OS,
			    MGMT_OS_CMD_ID_ECHO, zse);

	if (tx_smp_frame(buf_end - buf_smp) != 0)
		return 1;

	if (rx_smp_frame() != 0)
		return 1;

	if (mgmt_header_chk(MGMT_OP_WRITE,
			    MGMT_GROUP_ID_OS,
			    MGMT_OS_CMD_ID_ECHO) != 0)
		return 1;

	cbor_len = (size_t)(be16_to_cpu(hdr->nh_len));

	ZCBOR_STATE_D(zsd, 2, &buf_smp[CBOR_DATA_OFF], cbor_len, 1, 0);

	struct zcbor_map_decode_key_val echo_map[] = {
		ZCBOR_MAP_DECODE_KEY_DECODER("r", zcbor_tstr_decode, &rsp_r),
		ZCBOR_MAP_DECODE_KEY_DECODER("rc", zcbor_int32_decode, &rsp_rc),
	};

	ok = zcbor_map_decode_bulk(zsd, echo_map, ARRAY_SIZE(echo_map),
				   &decoded) == 0;

	if (!ok) {
		ERR(false, "CBOR decode error: %d", zcbor_peek_error(zsd));
		return 1;
	}

	if (rsp_r.len) {
		if (simple)
			fprintf(stdout, "response=%.*s\n", (int)rsp_r.len,
				rsp_r.value);
		else
			INF("received response: %.*s", (int)rsp_r.len,
			    rsp_r.value);
	} else if (rsp_rc != INT_MAX) {
		mgmt_status_display(rsp_rc);
		return 1;
	}

	return 0;
}

int
smp_mgmt_select(int slot)
{
	size_t cbor_len, decoded;
	int rsp_rc = INT_MAX;
	uint8_t *buf_end;
	bool ok;

	ZCBOR_STATE_E(zse, 0, &buf_smp[CBOR_DATA_OFF],
		      FRAME_BUF_LEN - CBOR_DATA_OFF, 0);

	ok = zcbor_map_start_encode(zse, 10)    &&
	     zcbor_tstr_put_lit(zse, tstr_slot) &&
	     zcbor_int32_put(zse, slot)         &&
	     zcbor_map_end_encode(zse, 10);

	if (!ok) {
		ERR(false, "CBOR encode error: %d", zcbor_peek_error(zse));
		return 1;
	}

	buf_end = smp_setup(MGMT_OP_WRITE, MGMT_GROUP_ID_OWRT,
			    MGMT_OWRT_CMD_ID_SLOTSEL, zse);

	if (tx_smp_frame(buf_end - buf_smp) != 0)
		return 1;

	if (rx_smp_frame() != 0)
		return 1;

	if (mgmt_header_chk(MGMT_OP_WRITE,
			    MGMT_GROUP_ID_OWRT,
			    MGMT_OWRT_CMD_ID_SLOTSEL) != 0)
		return 1;

	cbor_len = (size_t)(be16_to_cpu(hdr->nh_len));

	ZCBOR_STATE_D(zsd, 2, &buf_smp[CBOR_DATA_OFF], cbor_len, 1, 0);

	struct zcbor_map_decode_key_val select_map[] = {
		ZCBOR_MAP_DECODE_KEY_DECODER("rc", zcbor_int32_decode, &rsp_rc),
	};

	ok = zcbor_map_decode_bulk(zsd, select_map, ARRAY_SIZE(select_map),
				   &decoded) == 0;

	if (!ok) {
		ERR(false, "CBOR decode error: %d", zcbor_peek_error(zsd));
		return 1;
	}

	/* From SMP protocol specification: may not appear if 0 */
	if (decoded == 0)
		rsp_rc = MGMT_ERR_EOK;

	if (rsp_rc != MGMT_ERR_EOK) {
		mgmt_status_display(rsp_rc);
		return 1;
	}

	INF("request sent successfully");

	return 0;
}

int
smp_mgmt_simple(uint8_t mgmt_op, uint16_t mgmt_grp, uint8_t mgmt_id)
{
	size_t cbor_len, decoded;
	int rsp_rc = INT_MAX;
	uint8_t *buf_end;
	bool ok;

	ZCBOR_STATE_E(zse, 0, &buf_smp[CBOR_DATA_OFF],
		      FRAME_BUF_LEN - CBOR_DATA_OFF, 0);

	ok = zcbor_map_start_encode(zse, 1) &&
	     zcbor_map_end_encode(zse, 1);

	if (!ok) {
		ERR(false, "CBOR encode error: %d", zcbor_peek_error(zse));
		return 1;
	}

	buf_end = smp_setup(mgmt_op, mgmt_grp, mgmt_id, zse);

	if (tx_smp_frame(buf_end - buf_smp) != 0)
		return 1;

	if (rx_smp_frame() != 0)
		return 1;

	if (mgmt_header_chk(mgmt_op, mgmt_grp, mgmt_id) != 0)
		return 1;

	cbor_len = (size_t)(be16_to_cpu(hdr->nh_len));

	ZCBOR_STATE_D(zsd, 2, &buf_smp[CBOR_DATA_OFF], cbor_len, 1, 0);

	struct zcbor_map_decode_key_val select_map[] = {
		ZCBOR_MAP_DECODE_KEY_DECODER("rc", zcbor_int32_decode, &rsp_rc),
	};

	ok = zcbor_map_decode_bulk(zsd, select_map, ARRAY_SIZE(select_map),
				   &decoded) == 0;

	if (!ok) {
		ERR(false, "CBOR decode error: %d", zcbor_peek_error(zsd));
		return 1;
	}

	/* From SMP protocol specification: may not appear if 0 */
	if (decoded == 0)
		rsp_rc = MGMT_ERR_EOK;

	if (rsp_rc != MGMT_ERR_EOK) {
		mgmt_status_display(rsp_rc);
		return 1;
	}

	INF("request sent successfully");

	return 0;
}

int
smp_mgmt_sysinfo()
{
	struct zcbor_string rsp_soft_ver = { 0 }, rsp_zephyr_ver = { 0 };
	struct zcbor_string rsp_board = { 0 }, rsp_serial_num = { 0 };
	struct zcbor_string rsp_soc = { 0 }, rsp_target = { 0 };
	bool rsp_direct_xip, rsp_single_slot, ok;
	int rsp_active_slot, rsp_rc;
	size_t cbor_len, decoded;
	uint8_t *buf_end;

	ZCBOR_STATE_E(zse, 0, &buf_smp[CBOR_DATA_OFF],
		      FRAME_BUF_LEN - CBOR_DATA_OFF, 0);

	ok = zcbor_map_start_encode(zse, 1) &&
	     zcbor_map_end_encode(zse, 1);

	if (!ok) {
		ERR(false, "CBOR encode error: %d", zcbor_peek_error(zse));
		return 1;
	}

	buf_end = smp_setup(MGMT_OP_READ, MGMT_GROUP_ID_OWRT,
			    MGMT_OWRT_CMD_ID_SYSINFO, zse);

	if (tx_smp_frame(buf_end - buf_smp) != 0)
		return 1;

	if (rx_smp_frame() != 0)
		return 1;

	if (mgmt_header_chk(MGMT_OP_READ,
			    MGMT_GROUP_ID_OWRT,
			    MGMT_OWRT_CMD_ID_SYSINFO) != 0)
		return 1;

	cbor_len = (size_t)(be16_to_cpu(hdr->nh_len));

	ZCBOR_STATE_D(zsd, 2, &buf_smp[CBOR_DATA_OFF], cbor_len, 1, 0);

	rsp_active_slot = INT_MAX;
	rsp_rc = INT_MAX;

	rsp_single_slot = false;
	rsp_direct_xip = false;

	struct zcbor_map_decode_key_val sysinfo_map[] = {
		ZCBOR_MAP_DECODE_KEY_DECODER("active_slot", zcbor_int32_decode,
					     &rsp_active_slot),
		ZCBOR_MAP_DECODE_KEY_DECODER("board", zcbor_tstr_decode,
					     &rsp_board),
		ZCBOR_MAP_DECODE_KEY_DECODER("target", zcbor_tstr_decode,
					     &rsp_target),
		ZCBOR_MAP_DECODE_KEY_DECODER("soc", zcbor_tstr_decode,
					     &rsp_soc),
		ZCBOR_MAP_DECODE_KEY_DECODER("direct_xip", zcbor_bool_decode,
					     &rsp_direct_xip),
		ZCBOR_MAP_DECODE_KEY_DECODER("serial_num", zcbor_tstr_decode,
					     &rsp_serial_num),
		ZCBOR_MAP_DECODE_KEY_DECODER("single_slot", zcbor_bool_decode,
					     &rsp_single_slot),
		ZCBOR_MAP_DECODE_KEY_DECODER("soft_ver", zcbor_tstr_decode,
					     &rsp_soft_ver),
		ZCBOR_MAP_DECODE_KEY_DECODER("zephyr_ver", zcbor_tstr_decode,
					     &rsp_zephyr_ver),
		ZCBOR_MAP_DECODE_KEY_DECODER("rc", zcbor_int32_decode, &rsp_rc),
	};

	ok = zcbor_map_decode_bulk(zsd, sysinfo_map, ARRAY_SIZE(sysinfo_map),
				   &decoded) == 0;

	if (!ok) {
		ERR(false, "CBOR decode error: %d", zcbor_peek_error(zsd));
		return 1;
	}

	if (rsp_rc != INT_MAX) {
		mgmt_status_display(rsp_rc);
		return 1;
	}

	if (rsp_board.len) {
		if (simple)
			fprintf(stdout, "board=%.*s\n",
				(int)rsp_board.len, rsp_board.value);
		else
			fprintf(stdout, "Board: %.*s\n",
				(int)rsp_board.len, rsp_board.value);
	}

	if (rsp_target.len) {
		if (simple)
			fprintf(stdout, "target=%.*s\n",
				(int)rsp_target.len, rsp_target.value);
		else
			fprintf(stdout, "Target: %.*s\n",
				(int)rsp_target.len, rsp_target.value);
	}

	if (rsp_soc.len) {
		if (simple)
			fprintf(stdout, "soc=%.*s\n",
				(int)rsp_soc.len, rsp_soc.value);
		else
			fprintf(stdout, "SOC: %.*s\n",
				(int)rsp_soc.len, rsp_soc.value);
	}

	if (rsp_serial_num.len) {
		if (simple)
			fprintf(stdout, "serial_num=%.*s\n",
				(int)rsp_serial_num.len, rsp_serial_num.value);
		else
			fprintf(stdout, "Serial number: %.*s\n",
				(int)rsp_serial_num.len, rsp_serial_num.value);
	}

	if (rsp_soft_ver.len) {
		if (simple)
			fprintf(stdout, "soft_ver=%.*s\n",
				(int)rsp_soft_ver.len, rsp_soft_ver.value);
		else
			fprintf(stdout, "Software version: %.*s\n",
				(int)rsp_soft_ver.len, rsp_soft_ver.value);
	}

	if (rsp_zephyr_ver.len) {
		if (simple)
			fprintf(stdout, "zephyr_ver=%.*s\n",
				(int)rsp_zephyr_ver.len, rsp_zephyr_ver.value);
		else
			fprintf(stdout, "Zephyr version: %.*s\n",
				(int)rsp_zephyr_ver.len, rsp_zephyr_ver.value);
	}

	if (simple)
		fprintf(stdout, "direct_xip=%d\n", (int)rsp_direct_xip);
	else
		fprintf(stdout, "MCUboot in Direct-XIP mode: %s\n",
			rsp_direct_xip ? "yes" : "no");

	if (simple)
		fprintf(stdout, "single_slot=%d\n", (int)rsp_single_slot);
	else
		fprintf(stdout, "Single slot mode: %s\n",
			rsp_single_slot ? "yes" : "no");

	if (rsp_active_slot != INT_MAX) {
		if (simple)
			fprintf(stdout, "active_slot=%d\n", rsp_active_slot);
		else
			fprintf(stdout, "Active slot: %d\n", rsp_active_slot);
	}

	return 0;
}

int
smp_mgmt_upload(FILE *image, int image_len, int image_num)
{
	int image_off, read_len, rsp_off, rsp_rc, sha_len;
	char image_sha[IMAGE_SHA256_SIZE] = { 0 };
	char image_chunk[IMG_CHUNK_LEN] = { 0 };
	size_t cbor_len, decoded;
	uint8_t *buf_end;
	bool ok;

	/* Get image's hash */
	sha_len = image_sha256(image, image_len, image_sha, IMAGE_SHA256_SIZE);

	rewind(image);

	image_off = 0;
	while (image_off < image_len) {
		/* Read firmware in chunks... */
		read_len = fread(&image_chunk, 1, IMG_CHUNK_LEN, image);
		if (read_len == 0) {
			ERR(false, "firmware read failed");
			return 1;
		}

		ZCBOR_STATE_E(zse, 1, &buf_smp[CBOR_DATA_OFF],
			      FRAME_BUF_LEN - CBOR_DATA_OFF, 0);

		/* Image chunk offset */
		ok = zcbor_map_start_encode(zse, 10)   &&
		     zcbor_tstr_put_lit(zse, tstr_off) &&
		     zcbor_int32_put(zse, image_off);

		/* Only in first SMP packet: image number, len and sha */
		if (image_off == 0) {
			ok &= zcbor_tstr_put_lit(zse, tstr_img) &&
			      zcbor_int32_put(zse, image_num)   &&
			      zcbor_tstr_put_lit(zse, tstr_len) &&
			      zcbor_int32_put(zse, image_len);

			/* TODO: no SHA? */
			if (sha_len > 0) {
				ok &= zcbor_tstr_put_lit(zse, tstr_sha) &&
				      zcbor_bstr_encode_ptr(zse, image_sha,
							    sha_len);
			}
		}

		/* Image chunk data */
		ok &= zcbor_tstr_put_lit(zse, tstr_dat)                 &&
		      zcbor_bstr_encode_ptr(zse, image_chunk, read_len) &&
		      zcbor_map_end_encode(zse, 10);

		if (!ok) {
			ERR(false, "CBOR encode error: %d", zcbor_peek_error(zse));
			return 1;
		}

		buf_end = smp_setup(MGMT_OP_WRITE, MGMT_GROUP_ID_IMAGE,
				    MGMT_IMG_CMD_ID_UPLOAD, zse);

		if (tx_smp_frame(buf_end - buf_smp) != 0)
			return 1;

		if (rx_smp_frame() != 0)
			return 1;

		if (mgmt_header_chk(MGMT_OP_WRITE,
				    MGMT_GROUP_ID_IMAGE,
				    MGMT_IMG_CMD_ID_UPLOAD) != 0)
			return 1;

		cbor_len = (size_t)(be16_to_cpu(hdr->nh_len));

		ZCBOR_STATE_D(zsd, 2, &buf_smp[CBOR_DATA_OFF], cbor_len, 1, 0);

		rsp_rc = MGMT_ERR_EOK;
		rsp_off = INT_MAX;

		struct zcbor_map_decode_key_val upload_map[] = {
			ZCBOR_MAP_DECODE_KEY_DECODER("off", zcbor_int32_decode,
						     &rsp_off),
			ZCBOR_MAP_DECODE_KEY_DECODER("rc", zcbor_int32_decode,
						     &rsp_rc),
		};

		ok = zcbor_map_decode_bulk(zsd, upload_map, ARRAY_SIZE(upload_map),
					   &decoded) == 0;

		if (!ok) {
			ERR(false, "CBOR decode error: %d", zcbor_peek_error(zsd));
			return 1;
		}

		if (rsp_rc != MGMT_ERR_EOK) {
			mgmt_status_display(rsp_rc);
			return 1;
		}

		image_off += read_len;

		/* MCU reports back offset of last successfully written byte
		 * of update, we should check whether MCU doesn't requests
		 * image offset out of order and provide it */
		if (rsp_off != INT_MAX && rsp_off != image_off + IMG_CHUNK_LEN) {
			image_off = rsp_off;

			if (fseek(image, image_off, SEEK_SET) < 0) {
				ERR(false, "fseek on firmware failed");
				return 1;
			}
		}

		if (!quiet) {
			if (image_off != 0)
				fprintf(stdout, "\r");

			fprintf(stdout, "%d/%d", image_off, image_len);
			fflush(stdout);
		}
	}

	if (!quiet)
		fprintf(stdout, "\n");

	INF("done!");

	return 0;
}
