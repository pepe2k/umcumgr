/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright (C) 2022 Piotr Dymacz <pepe2k@gmail.com>
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <termios.h>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "image.h"
#include "umcumgr.h"
#include "smp.h"

#if defined(TI_SERIAL_BL_SUPPORT)
#include "tisbl.h"
#endif

int timeout = OPT_DEFAULT_TOUT;
static char *app_name;
bool simple = false;
bool quiet = false;
int uart_fd = -1;
#if defined(TI_SERIAL_BL_SUPPORT)
bool ti_sbl = false;
#endif

int
uart_tx_raw(uint8_t *buf, int len)
{
	int sent_len = 0;
	int out;

	do {
		out = write(uart_fd, &buf[sent_len], len - sent_len);
		if (out < 0)
			return 0;

		sent_len += out;
	} while (sent_len < len);

	return sent_len;
}

int
port_setup(int baudrate, bool hw_flow, struct termios *tio_backup)
{
	struct termios tio;
	speed_t speed;

	switch (baudrate) {
	case 9600:
		speed = B9600;
		break;
	case 19200:
		speed = B19200;
		break;
	case 38400:
		speed = B38400;
		break;
	case 57600:
		speed = B57600;
		break;
	case 115200:
		speed = B115200;
		break;
	case 230400:
		speed = B230400;
		break;
	case 460800:
		speed = B460800;
		break;
	case 500000:
		speed = B500000;
		break;
	case 576000:
		speed = B576000;
		break;
	case 921600:
		speed = B921600;
		break;
	case 1000000:
		speed = B1000000;
		break;
	case 1152000:
		speed = B1152000;
		break;
	case 1500000:
		speed = B1500000;
		break;
	case 2000000:
		speed = B2000000;
		break;
	case 2500000:
		speed = B2500000;
		break;
	case 3000000:
		speed = B3000000;
		break;
	case 3500000:
		speed = B3500000;
		break;
	case 4000000:
		speed = B4000000;
		break;
	default:
		ERR(false, "unsupported baud rate: %d", baudrate);
		return 1;
	}

	if (tcgetattr(uart_fd, tio_backup) != 0 ||
	    tcgetattr(uart_fd, &tio) != 0) {
		ERR(false, "tcgetattr() failed");
		return 1;
	}

	if (cfsetispeed(&tio, speed) < 0 ||
	    cfsetospeed(&tio, speed) < 0) {
		ERR(false, "baud rate set failed");
		return 1;
	}

	/* Control options */
	if (hw_flow)
		tio.c_cflag |= CRTSCTS;
	else
		tio.c_cflag &= ~CRTSCTS;

	tio.c_cflag &= ~(CSTOPB | PARENB | CSIZE);
	tio.c_cflag |= (CLOCAL | CREAD | CS8);

	/* Line/local options */
#if defined(TI_SERIAL_BL_SUPPORT)
	if (ti_sbl)
		tio.c_lflag &= ~(ICANON | ISIG);
	else
#endif
		tio.c_lflag |= (ICANON | ISIG);

	tio.c_lflag &= ~(ECHO | ECHOE | ECHONL | IEXTEN);

	/* Input options */
#if defined(TI_SERIAL_BL_SUPPORT)
	if (ti_sbl)
		tio.c_iflag &= ~IGNCR;
	else
#endif
		tio.c_iflag |= IGNCR;

	tio.c_iflag &= ~(INPCK | INLCR | ICRNL | IUCLC | IMAXBEL | IXON |
			 IXOFF | IXANY);

	/* Output options */
	tio.c_oflag &= ~OPOST;

	/* Continuation frame starts with 0x04 0x14, disable VEOF */
	tio.c_cc[VEOF] = 0;

	if (tcsetattr(uart_fd, TCSAFLUSH, &tio) < 0) {
		ERR(false, "tcsetattr() failed");
		return 1;
	}

	return 0;
}

void
usage(bool error)
{
	FILE *output_to = stdout;

	if (error)
		output_to = stderr;

	fprintf(output_to,
		"Usage:\n"
		"  %s [options] <command>\n"
		"\n"
		"Options:\n"
		"  -d <device>     Serial <device> path (default: %s)\n"
		"  -b <baud>       Use <baud> rate (default: %d)\n"
		"  -t <timeout>    Response <timeout> in seconds (default: %d)\n"
		"  -n <number>     Target image <number> to update (default: %d)\n"
#if defined(TI_SERIAL_BL_SUPPORT)
		"  -i              Texas Instruments serial bootloader mode\n"
#endif
		"  -f              Use hardware flow control\n"
		"  -s              Use script-friendly output (implies -q)\n"
		"  -h              Show this help and exit\n"
		"  -q              Quiet mode (don't print anything)\n"
		"\n"
		"Commands:\n"
		"  upload <image>  Upload <image> firmware to device\n"
		"  info <image>    Analyze <image> firmware file\n"
		"  hash <image>    Get hash embedded in <image> firmware file\n"
#if defined(TI_SERIAL_BL_DUMPCMD)
		"  dump <image>    Dump firmware from device to <image> (only for -i)\n"
#endif
#if defined(CONFIRM_TEST_SUPPORT)
		"  test <hash>     Test image with <hash> on next boot\n"
		"  confirm <hash>  Permanently switch to image with <hash>\n"
		"  confirm         Make current image setup permanent\n"
#endif
		"  echo <text>     Send and display echoed back <text>\n"
		"  select <slot>   Set <slot> to be active for booting\n"
		"  boot            Exit recovery and start firmware\n"
		"  list            List image(s) on device\n"
		"  reset           Reset the device\n"
		"  sysinfo         Get basic information about bootloader\n",
		app_name, OPT_DEFAULT_PORT, OPT_DEFAULT_BAUD, OPT_DEFAULT_TOUT,
		OPT_DEFAULT_SLOT);
}

int
main(int argc, char **argv)
{
	char img_sha[IMAGE_SHA256_SIZE] = { 0 };
	bool hwfc, port_set = false;
	int cmd, opt, sha_len, slot;
	struct termios tio_bckp;
	struct stat fw_st;
#if defined(CONFIRM_TEST_SUPPORT)
	char *hash = NULL;
#endif
	char *dev = NULL;
	char *txt = NULL;
	FILE *fw = NULL;
	uint32_t magic;
	int ret = 0;
	size_t len;
	int baud;

	baud = OPT_DEFAULT_BAUD;
	hwfc = OPT_DEFAULT_HWFC;
	slot = OPT_DEFAULT_SLOT;

	if (strrchr(argv[0], '/') != NULL)
		app_name = strrchr(argv[0], '/') + 1;
	else
		app_name = argv[0];

#if defined(TI_SERIAL_BL_SUPPORT)
	while ((opt = getopt(argc, argv, "fhiqsb:d:t:n:")) != -1) {
#else
	while ((opt = getopt(argc, argv, "fhqsb:d:t:n:")) != -1) {
#endif
		switch (opt) {
		case 'b':
			baud = atoi(optarg);
			if (!baud || baud < 0) {
				ERR(true, "incorrect baud rate: %s", optarg);
				return 1;
			}
			break;
		case 'd':
			dev = optarg;
			break;
		case 'f':
			hwfc = true;
			break;
#if defined(TI_SERIAL_BL_SUPPORT)
		case 'i':
			ti_sbl = true;
			break;
#endif
		case 'q':
			quiet = true;
			break;
		case 's':
			simple = true;
			quiet = true;
			break;
		case 't':
			timeout = atoi(optarg);
			if (!timeout || timeout < 0) {
				ERR(true, "incorrect timeout: %s", optarg);
				return 1;
			}
			break;
		case 'n':
			slot = atoi(optarg);
			if (slot < 0) {
				ERR(true, "incorrect slot: %s", optarg);
				return 1;
			}
			break;
		case 'h':
			usage(false);
			return 0;
		default:
			return 1;
		}
	}

	if (argc - optind < 1) {
		ERR(true, "no command parameter");
		return 1;
	}

	/* Check selected command */
	if (!strcasecmp(argv[optind], "boot"))
		cmd = CMD_BOO;
#if defined(CONFIRM_TEST_SUPPORT)
	else if (!strcasecmp(argv[optind], "confirm"))
		cmd = CMD_ACK;
#endif
#if defined(TI_SERIAL_BL_DUMPCMD)
	else if (!strcasecmp(argv[optind], "dump"))
		cmd = CMD_DMP;
#endif
	else if (!strcasecmp(argv[optind], "echo"))
		cmd = CMD_ECH;
	else if (!strcasecmp(argv[optind], "hash"))
		cmd = CMD_SHA;
	else if (!strcasecmp(argv[optind], "info"))
		cmd = CMD_INF;
	else if (!strcasecmp(argv[optind], "list"))
		cmd = CMD_LST;
	else if (!strcasecmp(argv[optind], "reset"))
		cmd = CMD_RST;
	else if (!strcasecmp(argv[optind], "select"))
		cmd = CMD_SEL;
	else if (!strcasecmp(argv[optind], "sysinfo"))
		cmd = CMD_SYS;
#if defined(CONFIRM_TEST_SUPPORT)
	else if (!strcasecmp(argv[optind], "test"))
		cmd = CMD_TST;
#endif
	else if (!strcasecmp(argv[optind], "upload"))
		cmd = CMD_UPL;
	else {
		ERR(true, "unknown command: %s", argv[optind]);
		return 1;
	}

#if defined(TI_SERIAL_BL_SUPPORT)
	/* Only subset of commands is supported in TI serial BL mode */
	if (ti_sbl && (cmd & ~(CMD_DMP | CMD_RST | CMD_UPL))) {
		ERR(false, "command unsupported in TI serial BL mode");
		return 1;
	}
#endif
#if defined(TI_SERIAL_BL_DUMPCMD)
	/* 'dump' command is currently supported only in TI serial BL mode */
	if (!ti_sbl && (cmd & CMD_DMP)) {
		ERR(false, "command supported only in TI serial BL mode");
		return 1;
	}
#endif

	/* Check selected device */
	if (cmd & (CMD_ACK | CMD_BOO | CMD_DMP | CMD_ECH | CMD_LST | CMD_RST |\
		   CMD_SEL | CMD_SYS | CMD_TST | CMD_UPL)) {
		if (!dev)
			dev = OPT_DEFAULT_PORT;

		uart_fd = open(dev, O_RDWR | O_NOCTTY);
		if (uart_fd < 0) {
			ERR(false, "failed to open: %s", dev);
			return 1;
		}

		if (port_setup(baud, hwfc, &tio_bckp)) {
			ret = 1;
			goto uart_out;
		}

		port_set = true;
	}

	/* Check selected firmware file */
	if (cmd & (CMD_DMP | CMD_INF | CMD_SHA | CMD_UPL)) {
		if (argc - optind < 2) {
			ERR(true, "no <image> parameter");

			ret = 1;
			goto uart_out;
		}

		if (cmd & CMD_DMP)
			fw = fopen(argv[optind + 1], "w+b");
		else
			fw = fopen(argv[optind + 1], "rb");

		if (!fw) {
			ERR(false, "failed to open: %s", argv[optind + 1]);

			ret = 1;
			goto uart_out;
		}

		/* Firmware size */
		if ((cmd & ~CMD_DMP) && fstat(fileno(fw), &fw_st) < 0) {
			ERR(false, "fstat() failed");

			ret = 1;
			goto fw_out;
		}

		/* Basic verification: check IMAGE_MAGIC */
		if (cmd & ~CMD_DMP) {
			len = fread(&magic, 1, 4, fw);
#if defined(TI_SERIAL_BL_SUPPORT)
			if (!ti_sbl && (len != 4 || le32_to_cpu(magic) !=
			    IMAGE_MAGIC)) {
#else
			if (len != 4 || le32_to_cpu(magic) != IMAGE_MAGIC) {
#endif
				ERR(false, "invalid image (too small or no magic)");

				ret = 1;
				goto fw_out;
			}
		}
	}

	/* Text for echo */
	if (cmd & CMD_ECH) {
		if (argc - optind < 2) {
			ERR(true, "no <text> parameter");

			ret = 1;
			goto uart_out;
		}

		txt = argv[optind + 1];

		if (strlen(txt) > OPT_MAX_ECHOLEN) {
			ERR(false, "text for echo too long");

			ret = 1;
			goto uart_out;
		}
	}

	/* Hash for confirm/test */
#if defined(CONFIRM_TEST_SUPPORT)
	if (cmd & (CMD_ACK | CMD_TST)) {
		if (argc - optind >= 2) {
			hash = argv[optind + 1];

			if (strlen(hash) != 2 * IMAGE_SHA256_SIZE) {
				ERR(false, "image hash should have %d characters",
					   2 * IMAGE_SHA256_SIZE);

				ret = 1;
				goto uart_out;
			}
		} else if (cmd & CMD_TST) {
			ERR(true, "no <hash> parameter");

			ret = 1;
			goto uart_out;
		}
	}
#endif

	/* Slot for select */
	if (cmd & CMD_SEL) {
		if (argc - optind < 2) {
			ERR(true, "no <slot> parameter");

			ret = 1;
			goto uart_out;
		}

		slot = atoi(argv[optind + 1]);
		if (slot < 0) {
			ERR(true, "incorrect slot: %s", argv[optind + 1]);
			return 1;
		}
	}

	switch (cmd) {
#if defined(CONFIRM_TEST_SUPPORT)
	case CMD_ACK:
		ret = smp_mgmt_confirm(hash, true);
		break;
#endif
	case CMD_BOO:
		ret = smp_mgmt_simple(MGMT_OP_WRITE, MGMT_GROUP_ID_OWRT,
				      MGMT_OWRT_CMD_ID_BOOT);
		break;
#if defined(TI_SERIAL_BL_DUMPCMD)
	case CMD_DMP:
		ret = tisbl_dump(fw);
		break;
#endif
	case CMD_ECH:
		ret = smp_mgmt_echo(txt);
		break;
	case CMD_INF:
		ret = image_info(fw, fw_st.st_size);
		break;
	case CMD_LST:
		ret = smp_mgmt_list();
		break;
	case CMD_RST:
#if defined(TI_SERIAL_BL_SUPPORT)
		if (ti_sbl)
			ret = tisbl_reset();
		else
#endif
			ret = smp_mgmt_simple(MGMT_OP_WRITE, MGMT_GROUP_ID_OS,
					      MGMT_OS_CMD_ID_SYSRST);
		break;
	case CMD_SEL:
		ret = smp_mgmt_select(slot);
		break;
	case CMD_SHA:
		sha_len = image_sha256(fw, fw_st.st_size, img_sha,
				       IMAGE_SHA256_SIZE);

		if (sha_len > 0) {
			if (simple)
				fprintf(stdout, "hash=");

			for (int i = 0; i < sha_len; i++)
				fprintf(stdout, "%02x",
					(unsigned char)img_sha[i]);

			fprintf(stdout, "\n");
		} else {
			ERR(false, "hash not found");
			ret = 1;
		}
		break;
	case CMD_SYS:
		ret = smp_mgmt_sysinfo();
		break;
#if defined(CONFIRM_TEST_SUPPORT)
	case CMD_TST:
		ret = smp_mgmt_confirm(hash, false);
		break;
#endif
	case CMD_UPL:
#if defined(TI_SERIAL_BL_SUPPORT)
		if (ti_sbl)
			ret = tisbl_upload(fw, fw_st.st_size);
		else
#endif
			ret = smp_mgmt_upload(fw, fw_st.st_size, slot);
		break;
	default:
		break;
	}

fw_out:
	if (fw)
		fclose(fw);

uart_out:
	if (uart_fd >= 0) {
		/* Restore old port settings */
		if (port_set)
			tcsetattr(uart_fd, TCSAFLUSH, &tio_bckp);

		close(uart_fd);
	}

	return ret;
}
